package com.example.databinding

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.databinding.databinding.ActivitySegundoBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivitySegundoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnMainActivity.setOnClickListener {
            accionBtn()
        }

    }

    fun accionBtn()
    {

        if(!etMainActivity.text.toString().isEmpty())
        {
            binding = DataBindingUtil.setContentView(getActivity(),R.layout.activity_segundo)
            binding.texto = Texto(etMainActivity.text.toString())
        } else{
            Toast.makeText(this,"Ingresa texto",Toast.LENGTH_SHORT).show()
        }

    }

}
